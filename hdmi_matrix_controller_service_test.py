"""Unit tests for HdmiMatrixControllerService."""
# pylint: disable=missing-docstring,invalid-name,too-many-public-methods,protected-access,no-self-use

import unittest

import mock
import paho.mqtt.client as mqtt

import hdmi_matrix_controller_service


class HdmiMatrixControllerServiceTest(unittest.TestCase):

    def test_handle_input_change_0(self):
        mock_mqtt_client = mock.Mock()
        mock_mqtt_client.publish = mock.MagicMock(return_value=(mqtt.MQTT_ERR_SUCCESS, 1))
        mock_trellis = mock.Mock()
        mock_matrix_controller = mock.Mock()
        matrix_index = 0

        hdmi_matrix_controller_service.handle_input_change(
            matrix_index, mock_mqtt_client, mock_trellis, mock_matrix_controller)

        mock_matrix_controller.change_port.assert_called_once_with(1, 1)

        mock_trellis.setLED.assert_called_once_with(0)
        mock_trellis.clrLED.assert_any_call(1)
        mock_trellis.clrLED.assert_any_call(2)
        mock_trellis.clrLED.assert_any_call(3)

    def test_handle_input_change_1(self):
        mock_mqtt_client = mock.Mock()
        mock_mqtt_client.publish = mock.MagicMock(return_value=(mqtt.MQTT_ERR_SUCCESS, 1))
        mock_trellis = mock.Mock()
        mock_matrix_controller = mock.Mock()
        matrix_index = 1

        hdmi_matrix_controller_service.handle_input_change(
            matrix_index, mock_mqtt_client, mock_trellis, mock_matrix_controller)

        mock_matrix_controller.change_port.assert_called_once_with(2, 1)

        mock_trellis.clrLED.assert_any_call(0)
        mock_trellis.setLED.assert_called_once_with(1)
        mock_trellis.clrLED.assert_any_call(2)
        mock_trellis.clrLED.assert_any_call(3)

    def test_handle_input_change_4(self):
        mock_mqtt_client = mock.Mock()
        mock_mqtt_client.publish = mock.MagicMock(return_value=(mqtt.MQTT_ERR_SUCCESS, 1))
        mock_trellis = mock.Mock()
        mock_matrix_controller = mock.Mock()
        matrix_index = 4

        hdmi_matrix_controller_service.handle_input_change(
            matrix_index, mock_mqtt_client, mock_trellis, mock_matrix_controller)

        mock_matrix_controller.change_port.assert_called_once_with(1, 2)

        mock_trellis.setLED.assert_called_once_with(4)
        mock_trellis.clrLED.assert_any_call(5)
        mock_trellis.clrLED.assert_any_call(6)
        mock_trellis.clrLED.assert_any_call(7)

    def test_handle_input_change_5(self):
        mock_mqtt_client = mock.Mock()
        mock_mqtt_client.publish = mock.MagicMock(return_value=(mqtt.MQTT_ERR_SUCCESS, 1))
        mock_trellis = mock.Mock()
        mock_matrix_controller = mock.Mock()
        matrix_index = 5

        hdmi_matrix_controller_service.handle_input_change(
            matrix_index, mock_mqtt_client, mock_trellis, mock_matrix_controller)

        mock_matrix_controller.change_port.assert_called_once_with(2, 2)

        mock_trellis.clrLED.assert_any_call(4)
        mock_trellis.setLED.assert_called_once_with(5)
        mock_trellis.clrLED.assert_any_call(6)
        mock_trellis.clrLED.assert_any_call(7)


if __name__ == '__main__':
    unittest.main()
