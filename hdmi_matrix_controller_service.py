"""Service for controlling a 4x4 HDMI matrix using the table buttons."""

import argparse
import logging
import random
import sys
#import threading
import time
import traceback

import paho.mqtt.client as mqtt
import serial
import rxv

import hdmi_matrix_controller

# Not necessary to have the library for testing.
if __name__ == '__main__':
    import Adafruit_Trellis  # pylint: disable=import-error

# Trellis info.
_KEYPAD_ROWS = 4
_KEYPAD_COLS = 4
_NUM_KEYS = _KEYPAD_ROWS * _KEYPAD_COLS
_DEBOUNCE_TIME = 0.5
_KEYPAD_MATRIX_ROWS = 4
_KEYPAD_LEFT_TV_ROW = 3
_KEYPAD_LEFT_TV_COL = 0
_KEYPAD_LIGHTS_ROW = 3
_KEYPAD_LIGHTS_COL = 1
_KEYPAD_RECEIVER_ROW = 3
_KEYPAD_RECEIVER_COL = 2
_KEYPAD_RIGHT_TV_ROW = 3
_KEYPAD_RIGHT_TV_COL = 3

# HDMI matrix controller settings.
_MATRIX_CONTROLLER_BAUD = 19200
_MATRIX_CONTROLLER_TIMEOUT = 1

# Arduino settings.
_ARDUINO_BAUD = 9600
_ARDUINO_LEFT_TV_CMD = '1\n'
_ARDUINO_RIGHT_TV_CMD = '2\n'

# MQTT server info.
_MQTT_USERNAME = 'sean'
_MQTT_PASSWORD = '<redacted>'
_MQTT_HOST = 'mosquitto.seanwatson.io'
_MQTT_CLIENT_ID = 'tablelights'
_MQTT_TLS_CA_FILE = '/home/pi/certs.pem'

# MQTT data for controlling HDMI matrix.
_MQTT_MATRIX_TOPIC_ROOT = 'homeautomation/hdmi_matrix/'
_MQTT_MATRIX_OUTPUT_PREFIX = 'output'

# MQTT data for controlling table lights.
_MQTT_LIGHTS_SWITCH_TOPIC = 'homeautomation/lights/tablelight/status/switch'
_MQTT_LIGHTS_COLOUR_TOPIC = 'homeautomation/lights/tablelight/rgb/set'
_MQTT_LIGHTS_ON_PAYLOAD = 'ON'
_MQTT_LIGHTS_OFF_PAYLOAD = 'OFF'
_MQTT_LIGHTS_COLORS = frozenset([
    '255,0,0',
    '0,255,0',
    '0,0,255',
    '255,255,0',
    '255,85,0',
    '255,0,255',
    '255,255,255',
    '0,255,255',
    '255,0,65',
    '35,255,0',
])


def row(num):
    """Get the row number for the given index.

    Args:
        num (int): Matrix index number.

    Returns:
        int: Row number for the given index.
    """
    return num / _KEYPAD_ROWS


def col(num):
    """Get the column number for the given index.

    Args:
        num (int): Matrix index number.

    Returns:
        int: Column number for the given index.
    """
    return num % _KEYPAD_COLS


def index(matrix_row, matrix_col):
    """Get the matrix index given a row and column.

    Args:
        matrix_row (int): Row in the matrix. 0 indexed.
        matrix_col (int): Column in the matrix. 0 indexed.

    Returns:
        int: The matrix index.
    """
    return matrix_row * _KEYPAD_COLS + matrix_col


def button_pressed(button_index, last_pressed_time, trellis):
    """Checks if a button has been pressed including debounce.

    Args:
        button_index (int): Button matrix index to check.
        last_pressed_time (int): Last timestamp of a button press.
        trellis (obj: Adafruit_Trellis.Adafruit_TrellisSet): Button matrix.

    Returns:
        bool: True if the button is pressed, False otherwise.
    """
    if trellis.justPressed(button_index):
        if time.time() > last_pressed_time + _DEBOUNCE_TIME:
            logging.debug('Button pressed. Row: %d, Column: %d',
                          row(button_index),
                          col(button_index))
            return True
    return False


def handle_input_change(button_index, mqtt_client, trellis, matrix_controller):
    """Changes the input based on the button pressed.

    Args:
        button_index (int): Button matrix index that was pressed.
        mqtt_client (obj: mqtt.Client): Connected and ready MQTT client.
        trellis (obj: Adafruit_Trellis.Adafruit_TrellisSet): Button matrix.
        matrix_controller (obj: hdmi_matrix_controller.HdmiMatrixController): Matrix controller.
    """
    logging.debug('Changing input for port %d to %d', row(button_index), col(button_index))
    try:
        matrix_controller.change_port(col(button_index) + 1,
                                      row(button_index) + 1)
    except (hdmi_matrix_controller.HdmiMatrixControllerException, ValueError) as exception:
        logging.error('Error changing input port: %s', exception)
    else:
        logging.debug('Input changed.')
        for i in range(_KEYPAD_COLS):
            if col(button_index) == i:
                trellis.setLED(button_index)
            else:
                trellis.clrLED(row(button_index) * _KEYPAD_COLS + i)
        trellis.writeDisplay()

        topic = (_MQTT_MATRIX_TOPIC_ROOT +
                 _MQTT_MATRIX_OUTPUT_PREFIX + str(row(button_index) + 1) +
                 '/status')
        (result, message_id) = mqtt_client.publish(
            topic, payload=str(col(button_index)), qos=1, retain=True)
        logging.debug('MQTT publishing %s to %s. Message id: %s.',
                      str(col(button_index)), topic, str(message_id))
        if result != mqtt.MQTT_ERR_SUCCESS:
            logging.error('Error publishing: %s.',
                          mqtt.error_string(result))


def handle_lights_change(turn_lights_on, mqtt_client, trellis):
    """Turns the table lights on or off.

    Args:
        turn_lights_on (bool): Set to True to turn lights on, False to turn them off.
        mqtt_client (obj: mqtt.Client): Connected and ready MQTT client.
        trellis (obj: Adafruit_Trellis.Adafruit_TrellisSet): Button matrix.
    """
    if turn_lights_on:
        logging.debug('Turning table lights on...')
        (result, message_id) = mqtt_client.publish(_MQTT_LIGHTS_SWITCH_TOPIC,
                                                   payload=_MQTT_LIGHTS_ON_PAYLOAD,
                                                   qos=1, retain=True)
        logging.debug('MQTT publishing %s to %s. Message id: %s.',
                      _MQTT_LIGHTS_ON_PAYLOAD, _MQTT_LIGHTS_SWITCH_TOPIC, str(message_id))
        if result != mqtt.MQTT_ERR_SUCCESS:
            logging.error('Error publishing: %s.',
                          mqtt.error_string(result))
            return

        color = random.choice(_MQTT_LIGHTS_COLORS)
        (result, message_id) = mqtt_client.publish(_MQTT_LIGHTS_COLOUR_TOPIC,
                                                   payload=color,
                                                   qos=1, retain=True)
        logging.debug('MQTT publishing %s to %s. Message id: %s.',
                      color, _MQTT_LIGHTS_COLOUR_TOPIC, str(message_id))
        if result != mqtt.MQTT_ERR_SUCCESS:
            logging.error('Error publishing: %s.',
                          mqtt.error_string(result))
            return

        trellis.setLED(index(_KEYPAD_LIGHTS_ROW, _KEYPAD_LIGHTS_COL))
        trellis.writeDisplay()
    else:
        logging.debug('Turning table lights off...')
        (result, message_id) = mqtt_client.publish(_MQTT_LIGHTS_SWITCH_TOPIC,
                                                   payload=_MQTT_LIGHTS_OFF_PAYLOAD,
                                                   qos=1, retain=True)
        logging.debug('MQTT publishing %s to %s. Message id: %s.',
                      _MQTT_LIGHTS_ON_PAYLOAD, _MQTT_LIGHTS_SWITCH_TOPIC, str(message_id))
        if result != mqtt.MQTT_ERR_SUCCESS:
            logging.error('Error publishing: %s.',
                          mqtt.error_string(result))
            return

        trellis.clrLED(index(_KEYPAD_LIGHTS_ROW, _KEYPAD_LIGHTS_COL))
        trellis.writeDisplay()


def handle_receiver_change(receiver, trellis):
    """Turns the receiver on or off.

    Args:
        receiver (obj: rxv.Rxv): Receiver control object.
        trellis (obj: Adafruit_Trellis.Adafruit_TrellisSet): Button matrix.
    """
    if receiver.on:
        logging.debug('Turning receiver off...')
        receiver.on = False

        trellis.clrLED(index(_KEYPAD_RECEIVER_ROW, _KEYPAD_RECEIVER_COL))
        trellis.writeDisplay()
    else:
        logging.debug('Turning receiver on...')
        receiver.on = True

        trellis.setLED(index(_KEYPAD_RECEIVER_ROW, _KEYPAD_RECEIVER_COL))
        trellis.writeDisplay()


def handle_tv_change(button_column, arduino_serial):
    """Turns the TVs on or off.

    Args:
        button_column (int): Column number of the button that was pressed.
        arduino_serial (obj: serial.Serial): Open serial device to write command to.
    """
    if button_column == _KEYPAD_LEFT_TV_COL:
        logging.debug('Toggling left TV power...')
        cmd = _ARDUINO_LEFT_TV_CMD
    elif button_column == _KEYPAD_RIGHT_TV_COL:
        logging.debug('Toggling right TV power...')
        cmd = _ARDUINO_RIGHT_TV_CMD
    else:
        return

    try:
        arduino_serial.write(cmd)
    except (serial.SerialException, serial.SerialTimeoutException) as exception:
        logging.error('Error writing to Arduino: %s', str(exception))


def cb_mqtt_on_connect(client, userdata, flags, response_code):
    """Callback for when MQTT connects to the server.

    Args:
      client (obj: mqtt.Client): MQTT client object.
      userdata (obj): Private userdata set at client creation time.
      flags (dict): Response flags.
      response_code (int): Connection result.
    """
    if response_code != mqtt.CONNACK_ACCEPTED:
        logging.error('Error connecting to MQTT server: %s.',
                      mqtt.connack_string(response_code))
        return

    logging.debug('MQTT connected.')
    (result, message_id) = client.subscribe(_MQTT_MATRIX_TOPIC_ROOT + '+/switch', qos=1)
    logging.debug('MQTT subscribing to: %s. Message id: %s.', _MQTT_MATRIX_TOPIC_ROOT + '+/switch',
                  str(message_id))
    if result != mqtt.MQTT_ERR_SUCCESS:
        logging.error('Error subscribing: %s.',
                      mqtt.error_string(result))


def cb_mqtt_on_disconnect(client, userdata, response_code):
    """Callback for when MQTT disconnects from the server.

    Args:
      client (obj: mqtt.Client): MQTT client object.
      userdata (obj): Private userdata set at client creation time.
      response_code (int): Connection result.
    """
    if response_code != mqtt.MQTT_ERR_SUCCESS:
        logging.error('Error disconnecting from MQTT broker: %s.',
                      mqtt.error_string(response_code))
    else:
        logging.debug('MQTT disconnected.')


def cb_mqtt_on_publish(client, userdata, message_id):
    """Callback for after MQTT publishes to a topic.

    For messages published with QOS 1 or 2 this callback means all the
    handshakes were completed successfully. For QOS 0 messages it means
    the message was queued to be sent, but it still may have failed.

    Args:
      client (obj: mqtt.Client): MQTT client object.
      userdata (obj): Private userdata set at client creation time.
      message_id (int): Key returned by publish call.
    """
    logging.debug('Successfully published to message ID: %s.', str(message_id))


def cb_mqtt_on_subscribe(client, userdata, message_id, granted_qos):
    """Callback for when MQTT subscribes to a topic.

    Args:
      client (obj: mqtt.Client): MQTT client object.
      userdata (obj): Private userdata set at client creation time.
      message_id (int): Key returned by subscribe call.
      granted_qos (list: int):  Granted QOS levels for each topic.
    """
    logging.debug('Successfully subscribed to message ID: %s. Grannted QOS: %s',
                  str(message_id), str(granted_qos))


def cb_mqtt_on_hdmi_input_changed(client, userdata, message):
    """Callback for when MQTT receives an HDMI input change message.

    Args:
      client: MQTT client object.
      userdata: Private userdata set at client creation time.
      message: The topic and payload that was published.
    """
    logging.debug('Got MQTT message from: %s', message.topic)
    hdmi_output = -1
    if _MQTT_MATRIX_OUTPUT_PREFIX + '1' in message.topic:
        hdmi_output = 0
    elif _MQTT_MATRIX_OUTPUT_PREFIX + '2' in message.topic:
        hdmi_output = 1
    elif _MQTT_MATRIX_OUTPUT_PREFIX + '3' in message.topic:
        hdmi_output = 2
    elif _MQTT_MATRIX_OUTPUT_PREFIX + '4' in message.topic:
        hdmi_output = 3
    else:
        logging.warning('Got message from unsupported output. %s', message.topic)
        return

    if int(message.payload) not in range(_KEYPAD_COLS):
        logging.error('Unrecognized payload: %s', message.payload)
        return
    hdmi_input = int(message.payload)

    logging.debug('Simulating button press of row %d, column %d.',
                  hdmi_output + 1, hdmi_input + 2)
    handle_input_change(index(hdmi_output, hdmi_input), client,
                        userdata.trellis, userdata.matrix_controller)


def parse_args():
    """Parses command line arguments.

    Returns:
        obj: argparse.Namespace: Parsed arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--matrix-serial-device', required=True,
                        help='File path to the HDMI matrix serial device.')
    parser.add_argument('-a', '--arduino-serial-device', required=True,
                        help='File path to the Arduino serial device.')
    parser.add_argument('-v', '--verbose', help='Increase output verbosity',
                        action='store_true')
    return parser.parse_args()


class MqttUserdata(object):  # pylint:disable=too-few-public-methods
    """Container for getting data into callbacks."""

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


def main():  # pylint: disable=too-many-branches,too-many-statements
    """Run the service."""
    args = parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.INFO)

    logging.info('Initializing button matrix...')
    matrix = Adafruit_Trellis.Adafruit_Trellis()
    trellis = Adafruit_Trellis.Adafruit_TrellisSet(matrix)
    trellis.begin((0x70, 1))  # I2C bus 1
    logging.info('Button matrix intialized.')

    logging.info('Initializing HDMI matrix serial device...')
    try:
        matrix_serial = serial.Serial(args.matrix_serial_device,
                                      _MATRIX_CONTROLLER_BAUD,
                                      timeout=_MATRIX_CONTROLLER_TIMEOUT)
    except (serial.SerialException, serial.SerialTimeoutException) as exception:
        logging.error('Failed to open serial device: %s', str(exception))
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)
    logging.info('HDMI matrix serial device initialized.')

    logging.info('Initializing HDMI matrix controller...')
    try:
        matrix_controller = hdmi_matrix_controller.HdmiMatrixController(matrix_serial)
        matrix_controller.set_beep(False)
    except (hdmi_matrix_controller.HdmiMatrixControllerException, ValueError) as exception:
        logging.error('Failed to initialize matrix controller: %s', str(exception))
        traceback.print_exc(file=sys.stdout)
        try:
            matrix_serial.close()
        except (serial.SerialException, serial.SerialTimeoutException):
            pass
        sys.exit(1)
    logging.info('HDMI matrix controller initialized.')

    logging.info('Initializing Arduino serial device...')
    try:
        arduino_serial = serial.Serial(args.arduino_serial_device,
                                       _ARDUINO_BAUD)
    except (serial.SerialException, serial.SerialTimeoutException) as exception:
        logging.error('Failed to open serial device: %s', str(exception))
        traceback.print_exc(file=sys.stdout)
        try:
            matrix_serial.close()
        except (serial.SerialException, serial.SerialTimeoutException):
            pass
        sys.exit(1)
    logging.info('Arduino serial device initialized.')

    logging.info('Initializing MQTT...')
    mqtt_userdata = MqttUserdata(
        trellis=trellis,
        matrix_controller=matrix_controller
    )
    mqtt_client = mqtt.Client(client_id=_MQTT_CLIENT_ID, clean_session=True)
    mqtt_client.tls_set(_MQTT_TLS_CA_FILE)
    mqtt_client.tls_insecure_set(False)
    mqtt_client.username_pw_set(_MQTT_USERNAME, _MQTT_PASSWORD)
    mqtt_client.user_data_set(mqtt_userdata)
    mqtt_client.on_connect = cb_mqtt_on_connect
    mqtt_client.on_disconnect = cb_mqtt_on_disconnect
    mqtt_client.on_publish = cb_mqtt_on_publish
    mqtt_client.on_subscribe = cb_mqtt_on_subscribe
    mqtt_client.message_callback_add(_MQTT_MATRIX_TOPIC_ROOT + '#', cb_mqtt_on_hdmi_input_changed)
    mqtt_client.connect(_MQTT_HOST)
    mqtt_client.loop_start()
    logging.info('MQTT initialized.')

    logging.info('Initializing receiver...')
    receiver = None
    receivers = rxv.find(timeout=3)
    if receivers:
        receiver = receivers[0]
        logging.info('Receiver initialized.')
    else:
        logging.info('No receiver found.')


    logging.info('Service starting...')
    lights_are_on = False
    last_button_press_time = time.time()
    while True:  # pylint: disable=too-many-nested-blocks
        try:
            if trellis.readSwitches():
                for i in range(_NUM_KEYS):
                    if button_pressed(i, last_button_press_time, trellis):
                        if row(i) < _KEYPAD_MATRIX_ROWS:
                            handle_input_change(i, mqtt_client, trellis, matrix_controller)
                        elif row(i) == _KEYPAD_LIGHTS_ROW and col(i) == _KEYPAD_LIGHTS_COL:
                            handle_lights_change(lights_are_on, mqtt_client, trellis)
                        elif row(i) == _KEYPAD_RECEIVER_ROW and col(i) == _KEYPAD_RECEIVER_COL:
                            if receiver is not None:
                                handle_receiver_change(receiver, trellis)
                        elif row(i) in (_KEYPAD_LEFT_TV_ROW, _KEYPAD_RIGHT_TV_ROW):
                            handle_tv_change(col(i), arduino_serial)
                        break
            else:
                time.sleep(0.2)

        except KeyboardInterrupt:
            logging.info('Exiting...')
            try:
                matrix_serial.close()
            except (serial.SerialException, serial.SerialTimeoutException):
                pass
            try:
                arduino_serial.close()
            except (serial.SerialException, serial.SerialTimeoutException):
                pass
            mqtt_client.loop_stop(force=True)
            mqtt_client.disconnect()
            sys.exit()
        except Exception as exception:  # pylint: disable=broad-except
            logging.error('Exception in main loop: %s', str(exception))
            traceback.print_exc(file=sys.stdout)
            try:
                matrix_serial.close()
            except (serial.SerialException, serial.SerialTimeoutException):
                pass
            try:
                arduino_serial.close()
            except (serial.SerialException, serial.SerialTimeoutException):
                pass
            mqtt_client.loop_stop(force=True)
            mqtt_client.disconnect()
            sys.exit(1)


if __name__ == '__main__':
    main()
