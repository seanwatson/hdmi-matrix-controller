from setuptools import setup

setup(
    name='HDMI Matrix Controller',
    version='0.1',
    description='Control an HDMI matrix.',
    author='Sean Watson',
    license='MIT',
    py_modules=['hdmi_matrix_controller', 'hdmi_matrix_controller_service'],
    install_requires=[
        'paho-mqtt',
        'rxv',
        'pyserial',
    ],
    zip_safe=False
)
